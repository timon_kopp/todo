$(document).ready(function(){
	
	$("#allgemein").click(function(){
		$("#panel_allg").slideToggle("slow");
		y = 0;
	});

	$("#juka").click(function(){
	    $("#panel_juka").slideToggle("slow");
	    y = 0;
	});


	$("#coffoom").click(function(){
		$("#panel_coffoom").slideToggle("slow");
		y = 0;
	});
	
	$("#addAllg").click(function(){
		$(".top").show();
		list_panel = "#list_allg";
		list_bearb = "#bearb_allg";
	});
	
	$("#addJuka").click(function(){
		$(".top").show();
		list_panel = "#list_juka";
		list_bearb = "#bearb_juka";
	});
	
	$("#addCoffoom").click(function(){
		$(".top").show();
		list_panel = "#list_coffoom";
		list_bearb = "#bearb_coffoom";
	});
	
	$("#ok").click(function(){
		  $(".top").hide();
		  titel = $("#titel").val();
		  x = $(list_panel).length;
		  if(y == 0 || y == x) {
			  $(list_panel).append('<li class="listeTitel" id="li' + x + '">' + titel + '</li>');
			  $(list_bearb).append('<li class="listeBearbeiten" id="be' + x + '"><form action=""><input type="button" class="editButton" id="bearbeiten' + x + '" value="Bearbeiten"></form></li>');
		  } else {
			  editEntry(list_panel, y, titel);
		  }
		  save(titel,list_panel);
		  $("#titel").val("");
		  return false;
	});

	$('.editButton').click(function(){
		y = this.id;
		loadEntry(list_panel, y, titel);
		$(".top").show();
	});
	
	$("#overlay").click(function(){
		$(".top").hide();
		return false;
	});

	$("#popup").click(function(){
		return false;
	});
	
	loadList("#list_allg", "bearb_allg");
	loadList("#list_juka", "bearb_juka");
	loadList("#list_coffoom", "bearb_coffoom");

});

