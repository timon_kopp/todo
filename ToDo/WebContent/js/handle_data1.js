


//Speichern
function save(item, list) {
  var list_a = getStoreArray(list);
  list_a.push(item);
  localStorage.setItem(list, JSON.stringify(list_a));
}

function getListLength(list) {
	list_a = getStoreArray(list);
	var x = list_a.length;
	return(x);
}

// get song names, then create a list element for each of them
function loadList(list, list_cat) {
  list_a = getStoreArray(list);
  list_c = $(list);
  if (list_a) {
    for (var i = 0; i < list_a.length; i++) {
    	if(list_a[i] != '') {
	    	cont = '<div class="liste" id="' + list_cat + i + '">' + list_a[i] + '</div>';
	    	$(list_c).append(cont);
    	}
    }
  }
  else{
	  alert("problem");
  }
}

function loadEntry(list, no, item) {
	list_a = getStoreArray(list);
	$(item).val(list_a[no]);
}

function editEntry(list, no, item) {
	list_a = getStoreArray(list);
	var temp = list_a[no];
	var c = $(list).html();
	c = c.replace(temp, item);
	$(list).html(c);
	list_a[no] = item;
	localStorage.setItem(list, JSON.stringify(list_a));
}

function deleteEntry(list, no, item) {
	list_a = getStoreArray(list);
	list_a[no] = '';
	localStorage.setItem(list, JSON.stringify(list_a));
}

function getStoreArray(key) {
	  // retrieve playlist from localstorage
	  var list = localStorage.getItem(key);

	  // if no object has been saved with the given key, initialize an empty array
	  if (!list)
	    list = new Array();
	  else
	    list = JSON.parse(list);

	  return list;
	}

