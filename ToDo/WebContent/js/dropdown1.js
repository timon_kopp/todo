$(document).ready(function(){
	
	$("#allgemein").click(function(){
		$("#panel_allg").slideToggle("slow");
	});

	$("#juka").click(function(){
	    $("#panel_juka").slideToggle("slow");
	});


	$("#coffoom").click(function(){
		$("#panel_coffoom").slideToggle("slow");
	});
	
	$("#addAllg").click(function(){
		$(".top").show("fast");
		$("#titel").focus();
		list_panel = "#inhaltAllg";
		list_cat = "allg";
		edit = 0;
	});
	
	$("#addJuka").click(function(){
		$(".top").show("fast");
		$("#titel").focus();
		list_panel = "#inhaltJuka";
		list_cat = "juka";
		edit = 0;
	});
	
	$("#addCoffoom").click(function(){
		$(".top").show("fast");
		$("#titel").focus();
		list_panel = "#inhaltCoffoom";
		list_cat = "coffoom";
		edit = 0;
	});
	
	$(":submit").click(function() {
		if($("#titel").val() != '') {
			$(".top").hide("fast");
			titel = $("#titel").val();
			if(edit == 0) {
				x = getListLength(list_panel);
				cont = '<div class="liste" id="' + list_cat + x + '">' + titel + '</div>';
				$(list_panel).append(cont);
				save(titel,list_panel);
			} else {
				editEntry('#' + inhalt, x, titel);
			}
			$("#titel").val("");
			edit = 0;
		} else {
			alert("Bitte einen Wert eintragen!");
	}
		return false;
	});
	
	$("#delete").click(function() {
		$(".top").hide("fast");
		x = temp.substr(temp.length -1);
		inhalt = $($('#' + temp).parent()).attr("id");
		$('#' + temp).remove();
		deleteEntry('#' + inhalt, x, '#titel');
		$("#titel").val("");
		return false;
	});

	$("div").on("click", ".liste", function(){
		temp = this.id;
		x = temp.substr(temp.length - 1);
		inhalt = $($('#' + temp).parent()).attr("id");
		loadEntry('#' + inhalt, x, "#titel");
		edit = 1;
		$(".top").show();
		$("#titel").focus();
		return false;
	});
	
	$("#overlay").click(function(){
		$(".top").hide("fast");
		$("#titel").val("");
		return false;
	});

	$("#popup").click(function(){
		return false;
	});
	
	loadList("#inhaltAllg", "allg");
	loadList("#inhaltJuka", "juka");
	loadList("#inhaltCoffoom", "coffoom");

});

function checkInhalt() {
	
}